<?php
use App\Models\ModelToken;

function akses_restapi($method, $url, $data, $email, $password)
{
    $client = \Config\Services::curlrequest();
    // $token  = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6InRpb211bHRhemVtQGdtYWlsLmNvbSIsImlhdCI6MTY2NTg1ODc0MCwiZXhwIjoxNjY1ODYyMzQwfQ.vfieSEnOmvXl6LK8QdjlIu43TNjEmCCQwbbeeZoz2cY";
    $model   = new ModelToken();
    $idToken = "1";
    $token   = $model->getToken($idToken);
    // echo $token;
    $tokenPart = explode(".", $token);
    $payload = $tokenPart[1];
    $decode = base64_decode($payload);
    $json = json_decode($decode, true);
    $exp = $json['exp'];
    $timeNow = time();
    if($exp <= $timeNow){
        $url = "http://localhost:8080/auth";
        $form_params = [
            'email'    => $email,
            'password' => $password
        ];
        $response = $client->request('post', $url, ['http_errors' => false, 'form_params' => $form_params]);
        $response = json_decode($response->getBody(), true);
        $token = $response['access_token'];
        $dataToken = [
            'id'=>$idToken,
            'token' =>$token
        ];
        $model->save($dataToken);
        // echo $response->getBody();
    }
    // echo $exp."--".$timeNow;

    $headers= [
        'Authorization' => 'Bearer ' . $token
    ];

    $response = $client->request($method, $url, ['headers' => $headers, 'http_errors' => false, 'form_params' => $data]);
    return $response->getBody();
    // setcookie('apeniii',$token);
}