<?php

namespace App\Controllers;

class ItemController extends BaseController
{
    public function item()
    {
        echo view('layout/header');
        echo view('item/item');
        echo view('layout/footer');
    }

    public function titip()
    {
        echo view('layout/header');
        echo view('item/titip');
        echo view('layout/footer');
    }

    public function pengajuan()
    {
        echo view('layout/header');
        echo view('item/pengajuan');
        echo view('layout/footer');
    }
}
