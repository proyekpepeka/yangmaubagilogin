<?php

namespace App\Controllers;

class Account extends BaseController
{
    public function login()
    {
        return view('account/login');
    }

    public function register()
    {
        return view('account/register');
    }

    public function menuakun()
    {
        echo view('layout/header');
        echo view('account/akun');
        echo view('layout/footer');
    }

    public function editprofil()
    {
        echo view('layout/header');
        echo view('account/editprofil');
        echo view('layout/footer');
    }

    public function logout()
    {
        echo view('layout/header');
        echo view('account/editprofil');
        echo view('layout/footer');
    }
}