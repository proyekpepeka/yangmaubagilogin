<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.104.2">
    <title>TitipAja</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/starter-template/">
    <link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/navbars/">


    
    <link href="/dist/css/bootstrap.min.css" rel="stylesheet">   
    <link href="/dist/template/starter-template.css" rel="stylesheet">
    <!-- <link href="/dist/template/navbar.css" rel="stylesheet"> -->
  </head>
  <body class="">
  <nav class="navbar navbar-expand-sm navbar-dark bg-dark" aria-label="Third navbar example">
    <div class="container-fluid">
      <a class="navbar-brand" href="/">TitipAja | Platform lelangnya anak STIS</a>
      <div class="collapse navbar-collapse" id="navbarsExample03">
        <ul class="navbar-nav me-auto mb-2 mb-sm-0">
          <li class="nav-item">
            <a class="nav-link" href="/item">Item</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/titip">Titip</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/pengajuan">Pengajuan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/akun">Akun</a>
          </li>
        </ul>
        <a class="btn btn-outline-danger" href="/logout">Log Out</a>
      </div>
    </div>
  </nav>


  