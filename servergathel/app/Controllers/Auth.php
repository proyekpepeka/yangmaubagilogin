<?php

namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use App\Models\ModelAuth;
use App\Models\ModelLogin;

class Auth extends BaseController
{
    use ResponseTrait;
    // function __construct()
    // {
    //     $this->model = new ModelAuth();
    // }
    public function index()
    {
        $validation = \Config\Services::validation();
        $rules      = [
            'email' => [
                'rules' => 'required|valid_email',
                'errors'=>[
                    'required'=> 'silakan masukkan email',
                    'valid_email' => 'silakan masukkan email yang valid'
                ]    
            ],
            'password' => [
                'rules' => 'required',
                'errors'=>[
                    'required'=> 'silakan masukkan password'
                ]
            ]
        ];
        $validation->setRules($rules);
        if(!$validation->withRequest($this->request)->run()){
            return $this->fail($validation->getErrors());
        }

        $model = new ModelAuth();
        $modelLogin = new ModelLogin();
        
        $email = $this->request->getVar('email');
        $password = $this->request->getVar('password');

        $data = $model->getEmail($email);
        if($data['password'] != md5($password)){
            return $this->fail("password tidak sesuai");
        }

        $modelLogin->saveEmail($email, $password);
        helper('jwt');
        $response = [
            'message'      => "otentikasi berhasil dilakukan",
            'data'         => $data,
            'access_token' => createJWT($email)
        ];
        $modelLogin->saveEmail($email, $password);
        return $this->respond($response);
    }
}
