<?php

namespace App\Models;

use App\Models\ModelAuth;
use CodeIgniter\Model;

class ModelLogin extends Model
{
    protected $table = "login";
    protected $allowedFields = ['email','password'];

    
    function saveEmail($email, $password)
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('login');
        
        // $this->model = new ModelAuth();
        // $data  = $this->model->getEmail($email);
        $data = 
        [
                'email'     =>$email,
                'password'  =>$password
        ];
        // $builder->emptyTable();
        $builder->insert($data);
        // var_dump($data2);
        return $data;
    }

    function getEmail()
    {
        $builder = $this->table("login")->first();
        return $builder;
    }
}