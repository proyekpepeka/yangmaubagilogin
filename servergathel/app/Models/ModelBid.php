<?php

namespace App\Models;

use CodeIgniter\Model;

class ModelTitipan extends Model{
    // protected $table = "users";
    protected $table = "barangtitipan";
    protected $pkey  = "id";
    protected $allowedFields = ['username','barang','harga','gambar','status',];

    protected $validationRules = [
        'username'  => 'required',
        'barang'    => 'required',
        'harga'     => 'required',
        'gambar'    => 'required',
        // 'status'    => 'required'
    ];

    protected $validationMessages = [
        'username' => [
            'required' => 'silakan masukkan username'
        ],

        'barang' => [
            'required' => 'silakan masukkan nama barang'
        ],
        'harga' => [
            'required' => 'silakan masukkan harga barang'
        ],
        'gambar' => [
            'required' => 'silakan unggah gambar barang yang sesuai'
        ],
        // 'status' => [
        //     'required' => 'silakan masukkan data kedua'
        // ]
    ];
}