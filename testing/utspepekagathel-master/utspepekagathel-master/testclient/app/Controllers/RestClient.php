<?php

namespace App\Controllers;

class RestClient extends BaseController
{
    public function index()
    {
        // $client = \Config\Services::curlrequest();
        // $token  = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6InRpb211bHRhemVtQGdtYWlsLmNvbSIsImlhdCI6MTY2NTgzNzYzMywiZXhwIjoxNjY1ODQxMjMzfQ.76NbAtZaH6u6JOGsxDTFt9eF9-NDVgepyM6ZYHgyilY";
        // // $url    = "http://localhost:8080/datauts/5";
        // $headers = [
        //     'Authorization' => 'Bearer ' . $token
        // ];
        // READ
        // $url    = "http://localhost:8080/datauts/5";
        // $response = $client->request('GET', $url, ['headers'=>$headers, 'http_errors' =>false]);
        // echo $response->getBody();
        
        // CREATE
        // $url    = "http://localhost:8080/datauts";
        // $data   = [
        //     'username'  => 'Ara',
        //     'data1'     => 'Sei Selayur',
        //     'data2'     => 'Polstat STIS'
        // ];
        // $response = $client->request('POST', $url, ['form_params'=>$data,'headers'=>$headers, 'http_errors' =>false]);

        // UPDATE
        // $url    = "http://localhost:8080/datauts/3";
        // $data   = [
        //     'username'  => 'Araaaaaaa',
        //     'data1'     => 'Sei Selayur',
        //     'data2'     => 'Polstat STIS'
        // ];
        // $response = $client->request('PUT', $url, ['form_params'=>$data,'headers'=>$headers, 'http_errors' =>false]);

        // DELETE
        // $url    = "http://localhost:8080/datauts/3";
        // $data   = [];
        // $response = $client->request('DELETE', $url, ['form_params'=>$data,'headers'=>$headers, 'http_errors' =>false]);
        // echo $response->getBody();

        helper(['restclient']);
        //READ dengan helper
        $url        = "http://localhost:8080/datauts";
        $data       = [];
        $response   = akses_restapi('get', $url, $data);
        $dataArray  = json_decode($response,true);
        foreach ($dataArray as $values)
        {
            echo "Username: " . $values['username'] . "<br/>";
            echo "Alamat  : " . $values['data1'] . "<br/>";
            echo "Kantor  : " . $values['data2'] . "<br/> <br/>";
        }
        
        // //CREATE dengan helper
        // $url    = "http://localhost:8080/datauts";
        // $data   =[
        //     'username'  => 'tiatio',
        //     'data1'     => 'rumah masa depan',
        //     'data2'     => 'BPS RI'
        // ];
        // $response = akses_restapi('POST', $url, $data);

        // //UPDATE dengan helper
        // $url    = "http://localhost:8080/datauts/4";
        // $data   =[
        //     'username'  => 'tiatio',
        //     'data1'     => 'karanganyar',
        //     'data2'     => 'BPS RI'
        // ];
        // $response = akses_restapi('PUT', $url, $data);

        // //DELETE dengan helper
        // $url    = "http://localhost:8080/datauts/4";
        // $data   =[];
        // $response = akses_restapi('DELETE', $url, $data);
        // echo $response;
    }
}
